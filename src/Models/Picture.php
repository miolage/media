<?php
namespace Collage\App\Models;

/**
 *
 */
class Picture extends Model {

    /**
     * @var int
     */
    public $id = 0;

    /**
     * @var int
     */
    public $collage_id = 0;

    /**
     * @var int
     */
    public $position = 0;

    /**
     * base64 encoded image string
     * @var string
     */
    public $img = '';

    /**
     * @var string
     */
    public $created_at = '';

    /**
     * @var string
     */
    public $updated_at = '';
}
