<?php
namespace Collage\App\Media;

use Collage\App\Models;
use \Gumlet\ImageResize;


/**
 */
class Controller {

	protected function sendHeader($str) {
		header($str);
	}

	/**
	 *
	 */
	public function dispatch($server) {
		if (!preg_match('/\/([0-9]+)\/([0-9]+)/', $server['REQUEST_URI'], $match)) {
			throw new \Exception('invalid request');
		}

		$this->sendHeader('Expires: ' . gmdate('D, d M Y H:i:s', strtotime("+30 day")) . ' GMT');

		$webp = $_GET['format'] == "webp";
		$image_type = $webp ? IMAGETYPE_WEBP : IMAGETYPE_JPEG;
		$this->sendHeader('Content-Type: ' . image_type_to_mime_type($image_type));

		$max_width = intval($_GET["width"]);
		$max_height = intval($_GET["height"]);
		$cache = $_GET['cache'];
		if (!preg_match('/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/', $cache)) {
			throw new \Exception("invalid cache parameter");
		}

		$version = 1;
		$id = [$version, $match[1], $match[2], $webp, $max_width, $max_height, strtotime($cache)];
		$filename = sys_get_temp_dir() . '/' . implode("_", $id);

		if (!file_exists($filename)) {
			$picture = Models\Picture::findBy("id", $match[2]);
			if (!$picture->collage_id) {
				throw new \Exception("invalid picture");
			}
			if ($picture->collage_id != $match[1]) {
				throw new \Exception("invalid collage id");
			}
			$image = ImageResize::createFromString(base64_decode(substr($picture->img, 23)));
			if ($webp) {
				$image->gamma_correct = false;
			}
			$image->crop($max_width, $max_height, true, ImageResize::CROPCENTER);
			$image->save($filename, $image_type);
			// see https://stackoverflow.com/questions/30078090/imagewebp-php-creates-corrupted-webp-files
			if ($webp) {
				if (filesize($filename) % 2 == 1) {
					file_put_contents($filename, "\0", FILE_APPEND);
				}
			}
		}

		return file_get_contents($filename);
	}
}