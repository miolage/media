<?php
require_once __DIR__ . '/vendor/autoload.php';

use Collage\App\Media;
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

$ctrl = new Media\Controller();
echo $ctrl->dispatch($_SERVER);
